defmodule Fridgebot.MixProject do
  use Mix.Project

  def project do
    [
      app: :fridgebot,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Fridgebot.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      {:socket, "~> 0.3"},
      {:httpoison, "~> 1.5"},
      {:nimble_parsec, "~> 0.6.0"},
      {:poison, "~> 4.0"},
      {:hunter, "~> 0.5.1"},
      {:ecto_sql, "~> 3.4.5"},
      {:postgrex, "~> 0.15.5"},
    ]
  end

  defp aliases do
    [test: ["ecto.create --quiet", "ecto.migrate", "test"]]
  end

end
