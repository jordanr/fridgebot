defmodule FoodTest do
  use ExUnit.Case

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Fridgebot.Repo)
  end

  alias Fridgebot.Storage.Food
  test "length requirements" do
    now = NaiveDateTime.utc_now()

    long = String.duplicate("e", 1+Application.fetch_env!(:fridgebot, :max_food_length))
    short = "e"
    empty = ""
    assert ! Enum.all?([long, short, empty], fn name ->
      Food.changeset(%Food{}, %{name: name, expiration: now}).valid?
    end)
    assert Food.changeset(%Food{}, %{name: "donut", expiration: now}).valid?
    assert Food.changeset(%Food{},
      %{name: "the regret that you have about your wasted youth",
        expiration: now}).valid?
                                     
  end
   

end

    
    
    
