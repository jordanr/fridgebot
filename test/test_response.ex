defmodule ResponseTest do
  use ExUnit.Case

  import Fridgebot.Responses

  test "every resonpse renders given enough assigns" do
    responses_list = Map.values(responses)
    for type <- responses_list,
      response <- type,
      fresh <- [true, false]
      do assert is_binary(
            EEx.eval_string(response, assigns: [user: "@test@test.social",
                                                food: "hotdog",
                                                fresh: fresh]))
    end
  end
end

      
      
