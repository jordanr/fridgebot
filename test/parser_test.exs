defmodule ParserTest do
  use ExUnit.Case

  alias Fridgebot.Parser

  def app_name, do: "@" <> Application.fetch_env!(:fridgebot, :username)

  defp flatten_get_results(string) do
    {:ok, results, _, _, _, _} = Parser.flatten_tags(string)
    Enum.join(results, "")
  end

  
  test "flatten tags with real input" do
    real_test = flatten_get_results("<p><span class=\"h-card\"><a href=\"https://botsin.space/@fridge\" class=\"u-url mention\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">@<span>fridge</span></a></span> how is it going today?</p>")

    assert real_test == "@fridge how is it going today?"
  end

  test "flatten tags with lots of nesting" do
    assert flatten_get_results("<a>hi <b>this is <i>very <b>very</b></i></b> nested</a>") == "hi this is very very nested"
  end
  
  test "flatten a link" do
    assert flatten_get_results(~s(<a href="https://example.com">example</a>)) == "example"
  end
  
  test "flatten a tag with lots of properties" do
    assert flatten_get_results(~s(<span t="b" d="a" f="1`12312124"><a t="12"><b>1</b></a></span>)) == "1"
  end
  
  test "flatten tags with stuff in the middle" do
    assert flatten_get_results(~s(<p><a></a>test1<b></b></p>)) == "test1"
  end
  
  test "flatten tags with a string at the end" do
    assert flatten_get_results(~s(<p><a></a> test2</p>)) == " test2"
  end
  
  test "flatten_tags with spaces between the tags in the stuff in the middle" do
    assert flatten_get_results(~s(<p><a></a>test3<b></b></p>)) == "test3"
  end
  
  test "Hungry command is parsed correctly" do
    assert Parser.command("#{app_name()} I'm hungry") == {:ok, [:hungry], "I'm hungry", %{}, {1, 0}, 8}
    end
end
