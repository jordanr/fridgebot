defmodule FridgebotTest do
  import Fridgebot
  use ExUnit.Case
  def appname, do: "@" <> Application.fetch_env!(:fridgebot, :username)

  test "I'm hungry command" do
    commands = ["I'm hungry", "i'm hungry",
     "I'm Hungry",
     "gimme something to eat", "give me something to eat"]
    assert(Enum.all? commands, fn command ->
      {type, _reply} = handle_mention("testuser", appname() <> command)
      type == :hungry
    end)
  end
end
