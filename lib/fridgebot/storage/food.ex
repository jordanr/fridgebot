defmodule Fridgebot.Storage.Food do
  use Ecto.Schema
  import Ecto.Changeset
  
  schema "yummies" do
    field :name, :string
    field :expiration, :naive_datetime
    timestamps()
  end

  defp remove_microsecond(food) do
    fetch_change!(food, :expiration)
    |> NaiveDateTime.truncate(:second)
    |> (&(change(food, expiration: &1))).()
  end

  def max_food_length() do
    Application.fetch_env!(:fridgebot, :max_food_length)
  end
  
  def changeset(food, attrs) do
    food 
    |> cast(attrs, [:name, :expiration])
    |> validate_required([:name, :expiration])
    |> validate_length(:name, min: 2,
    message: "That's too small")
    |> validate_length(:name, max: max_food_length(),
    message: "What kind of food is that long?")
    |> remove_microsecond
  end
end

