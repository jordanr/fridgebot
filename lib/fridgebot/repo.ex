defmodule Fridgebot.Repo do
  use Ecto.Repo,
    otp_app: :fridgebot,
    adapter: Ecto.Adapters.Postgres
end

