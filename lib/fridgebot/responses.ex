defmodule Fridgebot.Responses do

  def responses do
    %{hungry_no_food: 
    ["@<%= @user %>It looks like I'm fresh out of food at the moment, could you put some in maybe?",
     "@<%= @user %> Okay here you go... what?! I'm all empty",
    ],
  hungry_has_food: 
    ["here you go @<%= @user %>: <%= @food %>",
     "@<%= @user %> <%= if @fresh do %>Fresh as can be,\
<% else %>A little moldy but still good,<% end %>\
 I present to you <%= @food %>",
     "So, @<%= @user %>, how about <%= @food %>?",
     "This should fill your belly @<%= @user %>: <%= @food %>",
      "Fill the emptyness inside of you @<%= @user %> with this <%= if @fresh do %>fresh<% else %>old<% end %> <%= @food %>"],
  store: ["@<%= @user %> slap that <%= @food %> right inside of me",
          "@<%= @user %> my body is a temple and your <%= @food %> is more than welcome to enter it",
         ]
    }
  end

  def get_response(type, assigns) do
    responses()
    |> Map.get(type)
    |> Enum.random
    |> EEx.eval_string(assigns: assigns)
  end
end
