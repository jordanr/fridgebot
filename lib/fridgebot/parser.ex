defmodule Fridgebot.Parser do
  import NimbleParsec
  import Fridgebot.Parser.Helpers

  @max_food_length Application.fetch_env!(:fridgebot, :max_food_length)
 
  defparsec(:flatten_tags,
    ignore(opening())
    |> ignore(proplist())
    |> repeat(lookahead_not(string("</")) |> choice([text(), parsec(:flatten_tags)]))
    |> ignore(closing()))

  defcombinatorp(:store,
    ignore(app_name())
    |> ignore(repeat(ascii_char([?\s])))
    |> lookahead_not(hungry_command())
    |> replace(:store)
    |> concat(utf8_string([], max: @max_food_length)))
    
  defcombinatorp(:hungry,
    ignore(app_name())
    |> ignore(repeat(ascii_char([?\s])))
    |> lookahead(hungry_command())
    |> replace(:hungry))

  defparsec(:maybe_tags,
    choice([parsec(:flatten_tags), utf8_string([], max: 500)]))
    
  defparsec(:command,
    choice([parsec(:hungry), parsec(:store)]))
end
