defmodule Fridgebot.Mastodon.Stream do
  use GenServer
  alias Fridgebot.Mastodon

  require Logger
  @stream "api/v1/streaming/user"

  def init([]) do
    client = Fridgebot.Mastodon.Helpers.get_client
    url = client.base_url
    token = client.bearer_token
    location = Path.join(url, @stream)
    HTTPoison.request!(:get, location, "",
      [{"Authorization", "Bearer #{token}"}],
      stream_to: self(),
      recv_timeout: :infinity)
    {:ok, []}
  end

  def handle_info(%HTTPoison.AsyncStatus{code: 200}, state) do
    {:noreply, state}
  end

  def handle_info(%HTTPoison.AsyncStatus{code: not200}, _state) do
    raise "Recieved a code other than 200: #{not200}"
  end

  def handle_info(%HTTPoison.AsyncHeaders{}, state) do
    {:noreply, state}
  end

  def handle_info(%HTTPoison.AsyncChunk{chunk: data}, state) do
    case parse(data) do
      :ignore ->
        :ok
      %Hunter.Notification{type: "mention", status: status, account: account} ->
        from = account["acct"]
        msg = status["content"]
        Mastodon.handle_mention(from, msg)
      %Hunter.Notification{type: "follow", account: account} ->
        from = account["acct"]
        Mastodon.handle_follow(from)
        {:noreply, state}
    end
  end
  

  def handle_info(%HTTPoison.AsyncEnd{}) do
    Logger.debug("Reached end of chunked stream... exiting")
    exit(:normal)
  end
  
  def parse("event: notification\ndata: " <> rest) do
    Poison.decode!(rest, as: %Hunter.Notification{})
  end

  def parse(_) do
    :ignore
  end

  def start_link([]) do
    GenServer.start_link(__MODULE__, [])
  end

end
