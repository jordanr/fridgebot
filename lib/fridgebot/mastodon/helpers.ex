defmodule Fridgebot.Mastodon.Helpers do
  def get_client do
    base_url = Hunter.Config.api_base_url
    token = System.get_env("FRIDGEBOT_TOKEN")
    Hunter.new([base_url: base_url, bearer_token: token])
  end

  def verify(client) do
    Hunter.Account.verify_credentials(client)
  end
end

  
