defmodule Fridgebot.Storage do
  @moduledoc """ 
  The main interface to the fridgebot's storage.
  From here you do your basic CRUD for the food items stored within the fridge.
  """
  alias Fridgebot.Storage.Food
  alias Fridgebot.Repo
  alias NaiveDateTime, as: T
  import Ecto.Query, only: [from: 2] 

  @max_days 4

  defp add_days(time, days) do
    day_secs = 86400
    T.add(time, days * day_secs)
  end
  
  defp days_from_now(n) do
    T.utc_now() |> add_days(n)
  end


  @doc """
  Add a food to the database that expires in a number of days.
  If no days argument is given then a number of days between 1 and @max_days is
  chosen instead.
  One can also specify a rotten food item with [expired: days] that expired so many
  days previously.
  """
  def add_food(name, expires: days) do
    Food.changeset(%Food{}, %{name: name,
                              expiration: days_from_now(days)})
    |> Repo.insert()
  end
  
  def add_food(name, []) do
    some_days = days_from_now(:rand.uniform(@max_days))
    Food.changeset(%Food{}, %{name: name, expiration: some_days})
    |> Repo.insert()
  end

  def expired(max \\ nil) do
    now = T.utc_now()
    query = from food in Food,
      where: food.expiration < ^now,
      select: food,
      order_by: fragment("random ()")
    if max do
      require Ecto.Query
      query |> Ecto.Query.limit(^max)
    else
      query
    end
  end

  def get_one_food_item() do
    query = from food in Food,
      select: {food.name, food.expiration},
      order_by: fragment("random ()"),
      limit: 1
    Repo.one(query)
  end
  
end




