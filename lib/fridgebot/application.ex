defmodule Fridgebot.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Starts a worker by calling: Fridgebot.Worker.start_link(arg)
      # {Fridgebot.Worker, arg}
      Fridgebot.Mastodon.Stream,
      Fridgebot.Repo,
      Fridgebot.Mastodon,
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :rest_for_one, name: Fridgebot.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
