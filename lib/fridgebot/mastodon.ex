defmodule Fridgebot.Mastodon do
  use GenServer
  alias Fridgebot.Mastodon.Helpers
  require Logger
   
  @me __MODULE__
  
  def init(client) do
    Helpers.verify(client)
    {:ok, client}
  end

  def handle_call({:follow, from}, _caller, client) do
    Hunter.follow(client, from)
    {:reply, from, client}
  end

  def handle_call({:mention, from, message}, _caller, client) do
    {_, reply} = Fridgebot.handle_mention(from, message)
    Logger.debug("Sending #{reply}")
    %Hunter.Status{} = Hunter.create_status(client, reply)
    {:reply, reply, client}
  end

  def handle_info({_task, {:ok, reply}, client}) do
    Hunter.create_status(client, reply)
    {:noreply, client}
  end

  def handle_follow(from) do
    GenServer.call(@me, {:follow, from})
  end

  def handle_mention(from, message) do
    GenServer.call(@me, {:mention, from, message})
  end

  def start_link([]) do
    client = Helpers.get_client
    GenServer.start_link(@me, client, name: @me) 
  end
  
end

  
