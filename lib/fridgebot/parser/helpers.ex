defmodule Fridgebot.Parser.Helpers do
  import NimbleParsec
 
  def tag, do: ascii_string([?a..?z, ?A..?Z], min: 1)

  def proplist do
    repeat(
      times(ascii_char([?\s]), min: 1)
      |> concat(tag())
      |> string(~s(="))
      |> ascii_string([not: ?"], min: 0)
      |> ascii_char([?"])) 
  end

  def opening do
    string("<")
    |> concat(tag())
    |> concat(proplist())
    |> string(">")
  end
  

  def closing do
    string("</")
    |> concat(tag())
    |> string(">")
  end
  
  def text, do: ascii_string([not: ?<], min: 1)

  def app_name do
    "@" <> Application.fetch_env!(:fridgebot, :username) |> string()
  end

  def hungry_command do
    ["I'm hungry", "i'm hungry",
     "I'm Hungry",
     "gimme something to eat", "give me something to eat"]
    |> Enum.map(&string/1)
    |> choice()
  end
  
   
end

  
