defmodule Fridgebot do
  import Fridgebot.Responses
  alias Fridgebot.Parser
  
  def get_command(post) do
    {:ok, ws, _unparsed, _, _, _} = Parser.maybe_tags(post)
    Parser.command(Enum.join(ws))
  end
    
  def handle_mention(from, message) do
    get_command(message)
    |> clean_up
    |> evaluate_command(from)
  end

  def clean_up({:ok, [prog | args], unparsed, _, _, _}) do
    {prog, args, unparsed}
  end

  def evaluate_command({:hungry, [], _unparsed}, from) do
    case Fridgebot.Storage.get_one_food_item() do
      nil ->
        {:hungry, get_response(:hungry_no_food, [user: from])}
      {food, expiration} ->
        {:hungry, get_response(:hungry_has_food, [user: from,
                                              food: food,
                                              fresh: fresh?(expiration)])}
    end
  end

  def evaluate_command({:store, [food], _unparsed}, from) do
    case Fridgebot.Storage.add_food(food, []) do
      {:ok, _} ->
        {:store, get_response(:store, [user: from, food: food])}
      {:error, changset} ->
        # As it is now this will only ever print "That's too small"
        # The reason is that we ignore overflow so we won't try to
        # store anything that's too big
        {message, _change} = changset.errors[:name]
        {:error, message}
    end
  end

  def fresh?(expiration) do
    now = NaiveDateTime.utc_now()
    NaiveDateTime.compare(now, expiration) == :gt
  end
  
end



