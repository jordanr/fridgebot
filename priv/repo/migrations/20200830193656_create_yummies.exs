defmodule Fridgebot.Repo.Migrations.CreateYummies do
  use Ecto.Migration

  def change do
    create table(:yummies) do
      add :name, :string
      add :expiration, :naive_datetime
      timestamps()
    end
  end
end
