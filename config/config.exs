use Mix.Config

config :fridgebot,
  ecto_repos: [Fridgebot.Repo]

config :hunter,
  api_base_url: "https://botsin.space"

config :fridgebot,
  instance_app_name: "fridgebot"

config :fridgebot,
  username: "fridge"

config(:fridgebot, max_food_length: 100)

case Mix.env() do
  :dev ->
    config :fridgebot, Fridgebot.Repo,
    username: "postgres",
    password: "postgres",
    database: "fridgebot_dev",
    hostname: "localhost",
    show_sensitive_data_on_connection_error: true,
    pool_size: 10

  :test ->
    config :fridgebot, Fridgebot.Repo,
    username: "postgres",
    password: "postgres",
    database: "fridgebot_test",
    hostname: "localhost",
    show_sensitive_data_on_connection_error: true,
    pool: Ecto.Adapters.SQL.Sandbox

  :prod ->
    raise "Production environment is not set up"
  _ ->
    raise "This environment is not set up"
end

    



  

  


